# GitLab Homework Assignment

<https://www.youtube.com/watch?v=b--CWzopnz4>

See [`SLIDES.md`](SLIDES.md) for the contents of the presentation itself.

## Situation

You've been asked to present last minute at a conference and need to create a talk and supporting deck for that presentation. Please select a topic you are passionate about and create a presentation that will teach the audience the single most important thing they should know about this topic.

## Requirements

It can be about anything (and does not need to be about GitLab, software, or even business) but it must be presented with:

- A deck with at least 5 slides - Please have a clear thesis.
- Recorded to video with no editing, titles, or out post-production wizardry.
- Make sure your video is no longer than 5 minutes.
- Be sure to introduce yourself and have a conclusion to the presentation
- Upload it to YouTube (unlisted is fine) and send us the link within 48 hours.
- Show both your deck and face when presenting
- Tip: zoom works great for recording.

## Instructions

```sh
export PATH="$PATH:$(go env GOPATH)/bin"
go install github.com/maaslalani/slides@main

slides SLIDES.md
```
