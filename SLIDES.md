---
theme: dark
author: "@marshall007"
paging: "[ %d / %d ]"
---

# GitLab Homework Assignment

- **Role:** Strategy and Operations (Technical) Staff
- **Candidate:** [`@marshall007`](https://gitlab.com/marshall007)
- **Presentation:** https://gitlab.com/marshall007/gitlab-assignment

> You've been asked to present last minute at a conference and need to create a
> talk and supporting deck for that presentation. Please select a topic you are
> passionate about and create a presentation that will teach the audience
> the single most important thing they should know about this topic.

## Goals

- [x] Develop an understanding of scalability concerns for GitLab SaaS in <24hrs
- [x] Provide a an overview of proposed patterns
- [x] Attempt to identify an alternative solution

## Side Goals

- [x] Experiment with terminal-based presentation tooling
- [x] Presentation as markdown

---

# Database Scalability Working Group

<https://about.gitlab.com/company/team/structure/working-groups/database-scalability/>

> The charter of this working group is to produce a set of blueprints, design
> and initial implementation for scaling the database backend storage.

## Exit Criteria

- Accomodate 10M daily-active users (DAU) on GitLab.com
- Do not allow a problem with any given data-store to affect all users
- Minimize or eliminate complexity for our self-managed use-case

## Key Takeaways

1. Database sharding was explored last year, deemed impractical
1. New iteration expands scope of exploration to application layer
1. Wish to maintain single data-store as far down the stack as possible
1. Scaling database horizontally will increase latency, so will need caching
1. Must develop data access layer to mask that complexity from application

---

### Read-Mostly Data

_Data that is not updated often but read frequently._

**Examples:** `gitlab_subscriptions`,`license`, probably namespace/group membership

### Time-Decay Data

_Data which is accessed less frequently (or can be pruned entirely) as it ages._

**Examples:** `web_hook_logs`, `audit_events`, possibly `ci_builds`

### Entity/Service

_Decompose concrete entities into logical groups backed by separate data-stores._

**Examples:** Gitaly

### Sharding

_Widely used, but very difficult to implement. Shifts lots of complexity onto the application._

---

# Analysis: Proximity to DAU Target

The stated primary goal of the WG is to support `10M` daily-active users (DAU).
Without knowing the current DAU count, it is difficult to evaluate how close
we can get without introducing this additional complexity.

<https://about.gitlab.com/is-it-any-good/#gitlab-is-the-most-used-devops-tool>

- Total Registered Users (TRU) on GitLab.com: `6.2M` (as of July 2020)
- Current infrastructure is handling no more than `50%` of the target scale (`<10%`, see below)
- Already seeing database CPU utilization peak in the `60-90%` range somewhat regularly

<https://gitlab.com/search?search=%22performance+peak%22&project_id=1304532&scope=issues&sort=created_desc>

> **Update:** I was unable to find this at the time of the assignment, but
> monthly-active user (MAU) counts are public:
> <https://about.gitlab.com/handbook/product/performance-indicators/#unique-monthly-active-users-umau>
>
> As of July 2021 MAU count was `825k`, so currently supporting `<10%` of target scale.

---

# Analysis: Single Data-Store Philosphy

> GitLab uses a single data-store so you can get information about the whole
> software development lifecycle instead of parts of it. [...] Multiple
> data-stores lead to redundant and inconsistent data.

<https://about.gitlab.com/handbook/product/single-application/#single-data-store>

What we really mean by "single" is one _authoritative store for writes_.

GitLab.com already leverages multiple database engines and storage backends:

- Elasticsearch
- Redis
- In-Memory Cache
- Object Storage (blob storage)
- Gitaly (disk-backed storage)

Not in violation of design philosophy because (with the exception of file storage),
they act as read-only replicas.

---

# Multiple Data-Stores as a Feature?

> **I could not find any instances where this assumption was challenged.**

If we were to explore that option, Elasticsearch is an ideal candidate.

Difficult to justify adding a second database engine if scalability were the only factor,
but that is not the case for Elasticsearch:

- Already part of the stack (powers _Advanced Global Search_)
- Potential for consolidation of other parts of the stack (Prometheus / Grafana)
- First-class support for all identified scaling strategies (notably time-decay)
- Distinguishing functionality that could enhance existing product features
- Enablement of new product stages (i.e. `Monitor`)

---

```
  ██╗███╗░░██╗███╗░░██╗░█████╗░██╗░░░██╗░█████╗░████████╗███████╗
  ██║████╗░██║████╗░██║██╔══██╗██║░░░██║██╔══██╗╚══██╔══╝██╔════╝
  ██║██╔██╗██║██╔██╗██║██║░░██║╚██╗░██╔╝███████║░░░██║░░░█████╗░░
  ██║██║╚████║██║╚████║██║░░██║░╚████╔╝░██╔══██║░░░██║░░░██╔══╝░░
  ██║██║░╚███║██║░╚███║╚█████╔╝░░╚██╔╝░░██║░░██║░░░██║░░░███████╗
  ╚═╝╚═╝░░╚══╝╚═╝░░╚══╝░╚════╝░░░░╚═╝░░░╚═╝░░╚═╝░░░╚═╝░░░╚══════╝

████████╗░█████╗░░██████╗░███████╗████████╗██╗░░██╗███████╗██████╗░
╚══██╔══╝██╔══██╗██╔════╝░██╔════╝╚══██╔══╝██║░░██║██╔════╝██╔══██╗
░░░██║░░░██║░░██║██║░░██╗░█████╗░░░░░██║░░░███████║█████╗░░██████╔╝
░░░██║░░░██║░░██║██║░░╚██╗██╔══╝░░░░░██║░░░██╔══██║██╔══╝░░██╔══██╗
░░░██║░░░╚█████╔╝╚██████╔╝███████╗░░░██║░░░██║░░██║███████╗██║░░██║
░░░╚═╝░░░░╚════╝░░╚═════╝░╚══════╝░░░╚═╝░░░╚═╝░░╚═╝╚══════╝╚═╝░░╚═╝


 █▀▀ █ ▀█▀ █░░ ▄▀█ █▄▄   █▀▀ █▀█ █▀▄▀█ █▀▄▀█ █ ▀█▀   ▀█ █▀█ ▀█ ▀█
 █▄█ █ ░█░ █▄▄ █▀█ █▄█   █▄▄ █▄█ █░▀░█ █░▀░█ █ ░█░   █▄ █▄█ █▄ █▄
```
